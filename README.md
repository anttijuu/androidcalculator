# README #

Example Calculator app based on the ESDE course calculator exercise.


### What is this repository for? ###

* Demo of how to implement a calculator UI and functionality.
* 1.0.0
* [ESDE course](https://wiki.oulu.fi/display/esde/)

### How do I get set up? ###

* Clone
* Build
* Run

### Who do I talk to? ###

* Antti Juustila
* ESDE teacher team