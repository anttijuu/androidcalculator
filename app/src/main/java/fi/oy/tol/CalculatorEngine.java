/*
 * Copyright (c) 2014, Antti Juustila
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * The names of contributors may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fi.oy.tol;

import android.util.Log;

public class CalculatorEngine {
	
	public enum Operations {
		NOOP,
		MULTIPLY,
		DIVIDE,
		ADD,
		SUBTRACT,
		EQUALS,
	};
							 
	private double result = 0.0;
	private String enteredNumber = "";
	private boolean isEnteringNumbers = false;
	private Operations previousOperation = Operations.NOOP;
	private CalculatorEngineObserver observer = null;

	private static final String TAG = "CalcEngine";
	
	public CalculatorEngine(CalculatorEngineObserver anObserver) {
		observer = anObserver;
	}


	public void resetAll() {
		Log.d(TAG, "resetAll");
		result = 0.0;
		enteredNumber = "";
		previousOperation = Operations.NOOP;
		isEnteringNumbers = false;
		observer.calculatorStateUpdated();
	}

	public void clearNumber() {
		if (isEnteringNumbers) {
			if (enteredNumber.length() > 0) {
				Log.d(TAG, "Clear numbers");
				enteredNumber = enteredNumber.substring(0, enteredNumber.length()-1);
				observer.calculatorStateUpdated();
			}
		}
	}
	
	public String displayString() {
		if (isEnteringNumbers && enteredNumber.length() > 0) {
			Log.d(TAG, "Displaying currentNumber: " + enteredNumber);
			return stripZeroDecimal(Double.parseDouble(enteredNumber));
		} 
		Log.d(TAG, "Displaying result: " + result);
		return stripZeroDecimal(result);
	}
	
	private String stripZeroDecimal(double num) {
		double fractionalPart = num % 1;
		double integralPart = num - fractionalPart;
		if (fractionalPart == 0.0) {
			return Integer.toString((int)integralPart);
		}
		return Double.toString(num);
	}
	
	public double getResult() {
		return result;
	}
	
	public void setResult(double rslt) {
		result = rslt;
	}
	
	
	public void handleDigit(int aDigit) {
		handleDigit(Integer.toString(aDigit));
	}

	public void handleDigit(String aDigit) {
		isEnteringNumbers = true;
		enteredNumber += aDigit;
		observer.calculatorStateUpdated();
	}
	
	public void handleDecimalSeparator() {
		if (enteredNumber.indexOf('.') < 0) {
			if (enteredNumber.length() == 0) {
				enteredNumber = "0";
			}
			enteredNumber += ".";
			observer.calculatorStateUpdated();
		}
	}
	
	public void inverseNumber() {
		if (isEnteringNumbers) {
			double number;
			number = Double.parseDouble(enteredNumber);
			number = -number;
			enteredNumber = Double.toString(number);
		} else {
			result = -result;
		}
		observer.calculatorStateUpdated();
	}
	
	public void handleOperation(Operations op) {
		isEnteringNumbers = false;

		if (previousOperation == Operations.NOOP) {
			Log.d(TAG, "Handle op, previous is NOOP, currentNumber: " + enteredNumber);
			if (enteredNumber.length() > 0) {
				result = Double.parseDouble(enteredNumber);
			}
			enteredNumber = "";
			previousOperation = op;
		} else {
			if (enteredNumber.length() > 0) {
				double current = Double.parseDouble(enteredNumber);
				Log.d(TAG, "Number currently entered: " + current);
				switch (previousOperation) {
					case MULTIPLY: {
						result *= current;
						enteredNumber = "";
						previousOperation = op;
						Log.d(TAG, "********* MULTIPLY result: " + result);
						break;
					}
					case DIVIDE: {
						result /= current;
						enteredNumber = "";
						previousOperation = op;
						Log.d(TAG, "///////// DIVIDE result: " + result);
						break;
					}
					case ADD: {
						result += current;
						enteredNumber = "";
						previousOperation = op;
						Log.d(TAG, "+++++++++ ADD result: " + result);
						break;
					}
					case SUBTRACT: {
						result -= current;
						enteredNumber = "";
						previousOperation = op;
						Log.d(TAG, "--------- SUBTRACT result: " + result);
						break;
					}
					case EQUALS: {
						Log.d(TAG, "========= EQUALS result: " + result);
						Operations tmpOp = previousOperation;
						previousOperation = Operations.NOOP;
						handleOperation(tmpOp);
						break;
					}
				}
			} else {
				Log.d(TAG, "No currently entered num, saving op to prev op" + result);
				previousOperation = op;
			}
		}
		observer.calculatorStateUpdated();
	}
	
}
