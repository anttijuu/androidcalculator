/*
 * Copyright (c) 2014, Antti Juustila
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * The names of contributors may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fi.oy.tol;

import fi.oy.tol.CalculatorEngine.Operations;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;


public class CalculatorActivity extends Activity implements OnClickListener, CalculatorEngineObserver {

	private CalculatorEngine calculator = null;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		calculator = new CalculatorEngine(this);

		Button button =  (Button) findViewById(R.id.buttonDivision);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.buttonMultiplication);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.buttonPlus);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.buttonMinus);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.buttonEquals);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.buttonAC);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.buttonClear);
		button.setOnClickListener(this);

		button =  (Button) findViewById(R.id.buttonDecimalSeparator);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.buttonPlusMinus);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.button0);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.button1);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.button2);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.button3);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.button4);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.button5);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.button6);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.button7);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.button8);
		button.setOnClickListener(this);
		button =  (Button) findViewById(R.id.button9);
		button.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		restoreState();
		calculatorStateUpdated();
	}

	@Override
	protected void onPause() {
		super.onPause();
		saveState();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		calculator = null;
	}

	@Override
	public void calculatorStateUpdated() {
		String result = calculator.displayString();
		TextView resultView = (TextView) findViewById(R.id.resultNumber);
		resultView.setText(result);
	}

	@Override
	public void onClick(View view) {
		int buttonId = view.getId();
		if (buttonId == R.id.button0) {
			calculator.handleDigit(0);
		} else if (buttonId == R.id.button1) {
			calculator.handleDigit(1);
		} else if (buttonId == R.id.button2) {
			calculator.handleDigit(2);
		} else if (buttonId == R.id.button3) {
			calculator.handleDigit(3);
		} else if (buttonId == R.id.button4) {
			calculator.handleDigit(4);
		} else if (buttonId == R.id.button5) {
			calculator.handleDigit(5);
		} else if (buttonId == R.id.button6) {
			calculator.handleDigit(6);
		} else if (buttonId == R.id.button7) {
			calculator.handleDigit(7);
		} else if (buttonId == R.id.button8) {
			calculator.handleDigit(8);
		} else if (buttonId == R.id.button9) {
			calculator.handleDigit(9);
		} else if (buttonId == R.id.buttonDecimalSeparator) {
			calculator.handleDecimalSeparator();
		} else if (buttonId == R.id.buttonPlusMinus) {
			calculator.inverseNumber();
		} else if (buttonId == R.id.buttonDivision) {
			calculator.handleOperation(Operations.DIVIDE);
		} else if (buttonId == R.id.buttonMultiplication) {
			calculator.handleOperation(Operations.MULTIPLY);
		} else if (buttonId == R.id.buttonPlus) {
			calculator.handleOperation(Operations.ADD);
		} else if (buttonId == R.id.buttonMinus) {
			calculator.handleOperation(Operations.SUBTRACT);
		} else if (buttonId == R.id.buttonEquals) {
			calculator.handleOperation(Operations.EQUALS);
		} else if (buttonId == R.id.buttonAC) {
			calculator.resetAll();
		} else if (buttonId == R.id.buttonClear) {
			calculator.clearNumber();
		}
	}


	private void saveState() {
		FileOutputStream outputStream;

		try {
			outputStream = openFileOutput(RESULT, Context.MODE_PRIVATE);
			byte[] bytes = new byte[8];
			ByteBuffer.wrap(bytes).putDouble(calculator.getResult());
			outputStream.write(bytes);
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreState() {
		FileInputStream inputStream;
		try {
			inputStream = openFileInput(RESULT);
			byte[] bytes = new byte[8];
			inputStream.read(bytes);
			calculator.setResult(ByteBuffer.wrap(bytes).getDouble());
			inputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private final static String RESULT = "result";

}